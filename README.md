# Dependency Injection in Jersey 2

This repository contains multiple ways of doing dependency injection in jersey 2.

## On Java SE with Weld

This branch shows how to do CDI with Weld on Java SE with grizzly embedded server.