package org.metastringfoundation.starters.jaxrs;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloResourceTest {

    private static Server server;
    private final Client client = ClientBuilder.newClient();

    @BeforeAll
    public static void setupServer() throws IOException {
        server = Server.serverWithClasses(HelloResource.class);
        server.start();
    }

    @AfterAll
    public static void stopServer() {
        server.shutdown();
    }

    @Test
    public void applicationScopedCounterReturnsDifferentNumbersEveryTime() {
        Integer response1 = client.target(Server.BASE_URI).path("/count").request().get(Integer.class);
        Integer response2 = client.target(Server.BASE_URI).path("/count").request().get(Integer.class);
        assertEquals(1, response1);
        assertEquals(2, response2);
    }

    @Test
    public void requestScopedCounterReturnsNewStringEveryTime() {
        String response1 = client.target(Server.BASE_URI).path("/greet/morning").request().get(String.class);
        String response2 = client.target(Server.BASE_URI).path("/greet/night").request().get(String.class);
        assertEquals("Good morning!", response1);
        assertEquals("Good night!", response2);
    }
}
