package org.metastringfoundation.starters.jaxrs;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/")
public class HelloResource {
    Counter counter;

    Greeter greeter;

    @Inject
    public HelloResource(Counter counter, Greeter greeter) {
        this.counter = counter;
        this.greeter = greeter;
    }

    @GET
    @Path("count")
    public Integer getCount() {
        return counter.getCount();
    }

    @GET
    @Path("greet/{time}")
    public String getGreeting(@PathParam("time") String timeOfDay) {
        return greeter.getGreeting(timeOfDay);
    }
}
