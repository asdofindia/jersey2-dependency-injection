package org.metastringfoundation.starters.jaxrs;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.PathParam;

@RequestScoped
public class GreeterUser implements Greeter {
    @Override
    public String getGreeting(String timeOfDay) {
        return "Good " + timeOfDay + "!";
    }
}
