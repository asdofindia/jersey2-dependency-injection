package org.metastringfoundation.starters.jaxrs;

public interface Counter {
    int getCount();
}
