package org.metastringfoundation.starters.jaxrs;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CounterGlobal implements Counter {
    private int calledCount;

    public CounterGlobal() {
        calledCount = 0;
    }

    public int getCount() {
        calledCount += 1;
        return calledCount;
    }
}
