package org.metastringfoundation.starters.jaxrs;

public interface Greeter {
    String getGreeting(String timeOfDay);
}
