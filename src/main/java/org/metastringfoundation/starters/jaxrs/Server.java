package org.metastringfoundation.starters.jaxrs;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class Server {
    static String BASE_URI = "http://localhost:9091";
    private ResourceConfig resourceConfig = new ResourceConfig();
    private HttpServer httpServer;

    public Server() {}

    public ResourceConfig getResourceConfig() {
        return resourceConfig;
    }

    public void setResourceConfig(ResourceConfig resourceConfig) {
        this.resourceConfig = resourceConfig;
    }

    public static void main(String[] args) throws IOException {
       Server server = defaultServer();
       server.start();
    }

    public static Server defaultServer() {
        Server server = new Server();
        server.getResourceConfig().packages("org.metastringfoundation.starters.jaxrs");
        return server;
    }

    public void shutdown() {
        httpServer.shutdown();
    }


    public static Server serverWithClasses(Class<?>... classes) {
        Server server = new Server();
        server.getResourceConfig().registerClasses(classes);
        return server;
    }

    public void start() throws IOException {
        httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), resourceConfig, false);
        Runtime.getRuntime().addShutdownHook(new Thread(httpServer::shutdown));
        httpServer.start();

        System.out.println("Application started.\nTry out " + BASE_URI);
    }
}
